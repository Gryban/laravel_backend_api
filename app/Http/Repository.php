<?php
/**
 * Date: 28.12.17
 * Time: 18:58
 */

namespace App\Http;

use \Optimus\Genie\Repository as BaseRepository;

/**
 * Class Repository
 *
 * @package App\Http
 * @author  Oleksii Griban
 */
class Repository extends BaseRepository
{
    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }
}