<?php
/**
 * Date: 23.05.18
 * Time: 11:17
 */

namespace App\Http\Auth\Proxy;

use App\User;

/**
 * Class LoginProxy
 *
 * @package App\Http\Auth
 * @author  Oleksii Griban
 */
class LoginProxy
{
    /**
     * @param User $user
     * @return string
     */
    public function token(User $user) : string
    {
        return $user->createToken('Laravel')->accessToken;
    }

    public function refresh()
    {
        //
    }
}