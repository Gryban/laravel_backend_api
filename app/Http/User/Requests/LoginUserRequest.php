<?php
/**
 * Date: 28.12.17
 * Time: 18:05
 */

namespace App\Http\User\Requests;

use App\Http\Request;

/**
 * Class LoginUserRequest
 *
 * @package App\Http\User\Requests
 * @author  Oleksii Griban
 */
class LoginUserRequest extends Request
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'email'    => 'required|string|email|max:255',
            'password' => 'required|string|max:45|min:6'
        ];
    }
}