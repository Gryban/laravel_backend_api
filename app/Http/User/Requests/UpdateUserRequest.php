<?php
/**
 * Date: 28.12.17
 * Time: 18:05
 */

namespace App\Http\User\Requests;

use App\Http\Request;

/**
 * Class UpdateUserRequest
 *
 * @package App\Http\User\Requests
 * @author  Oleksii Griban
 */
class UpdateUserRequest extends Request
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255|min:3'
        ];
    }
}