<?php
/**
 * Date: 28.12.17
 * Time: 18:05
 */

namespace App\Http\User\Requests;

use App\Http\Request;

/**
 * Class CreateUserRequest
 *
 * @package App\Http\User\Requests
 * @author  Oleksii Griban
 */
class CreateUserRequest extends Request
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'email'    => 'required|string|email|max:255|unique:users',
            'name'     => 'required|string|max:255|min:3',
            'password' => 'required|string|max:45|min:6'
        ];
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return [
            'user.email' => 'the user\'s email'
        ];
    }
}