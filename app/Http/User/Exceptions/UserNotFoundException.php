<?php
/**
 * Created by Skynix Team
 * Date: 30.03.18
 * Time: 16:38
 */

namespace Api\Users\Exceptions;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class UserNotFoundException
 *
 * @package Api\Users\Exceptions
 * @author  Oleksii Griban
 */
class UserNotFoundException extends NotFoundHttpException
{
    /**
     * UserNotFoundException constructor.
     */
    public function __construct()
    {
        parent::__construct('The user was not found.');
    }
}