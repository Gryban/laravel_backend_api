<?php
/**
 * Date: 28.12.17
 * Time: 18:03
 */

namespace App\Http\User\Services;

use Api\Users\Exceptions\UserNotFoundException;
use App\Http\Auth\Proxy\LoginProxy;
use App\Http\User\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;

/**
 * Class LoginUserService
 *
 * @package App\Http\User\Services
 * @author  Oleksii Griban
 */
class LoginUserService
{
    /** @var UserRepository  */
    private $userRepository;

    /** @var LoginProxy  */
    private $loginProxy;

    /**
     * LoginUserService constructor.
     *
     * @param UserRepository $userRepository
     * @param LoginProxy     $loginProxy
     */
    public function __construct(
        UserRepository $userRepository,
        LoginProxy $loginProxy
    ) {
        $this->userRepository = $userRepository;
        $this->loginProxy = $loginProxy;
    }

    /**
     * @param $data
     * @return UserNotFoundException|string
     */
    public function login($data)
    {
        if(Auth::attempt(['email' => $data['email'], 'password' => $data['password']])) {
            $user = Auth::user();
            return ['access_token' => $this->loginProxy->token($user)];
        }

        return new UserNotFoundException();
    }
}