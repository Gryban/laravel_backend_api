<?php
/**
 * Date: 28.12.17
 * Time: 18:03
 */

namespace App\Http\User\Services;

use App\Http\User\Repositories\UserRepository;
use Illuminate\Events\Dispatcher;

/**
 * Class FetchUserService
 *
 * @package App\Http\User\Services
 * @author  Oleksii Griban
 */
class FetchUserService
{
    /** @var UserRepository  */
    private $userRepository;

    /**
     * CreateUserService constructor.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(
        UserRepository $userRepository
    ) {
        $this->userRepository = $userRepository;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function fetch()
    {
        return $this->userRepository->get();
    }
}