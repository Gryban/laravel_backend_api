<?php
/**
 * Date: 28.12.17
 * Time: 18:03
 */

namespace App\Http\User\Services;

use App\Http\Auth\Proxy\LoginProxy;
use App\Http\User\Repositories\UserRepository;
use App\User;
use Illuminate\Events\Dispatcher;

/**
 * Class UpdateUserService
 *
 * @package App\Http\User\Services
 * @author  Oleksii Griban
 */
class UpdateUserService
{
    /** @var Dispatcher  */
    private $dispatcher;

    /** @var UserRepository  */
    private $userRepository;

    /**
     * CreateUserService constructor.
     *
     * @param Dispatcher     $dispatcher
     * @param UserRepository $userRepository
     */
    public function __construct(
        Dispatcher $dispatcher,
        UserRepository $userRepository
    ) {
        $this->dispatcher = $dispatcher;
        $this->userRepository = $userRepository;
    }

    /**
     * @param $id
     * @param $request
     * @return mixed
     */
    public function update($id, $request)
    {
        return ['userId' => $this->userRepository->update($id, $request)];
    }
}