<?php
/**
 * Created by Skynix Team
 * Date: 28.12.17
 * Time: 18:03
 */

namespace App\Http\User\Services;

use App\Http\User\Repositories\UserRepository;

/**
 * Class ViewUserService
 *
 * @package App\Http\User\Services
 * @author  Oleksii Griban
 */
class ViewUserService
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * ViewUserService constructor.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param int $userId
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function view(int $userId)
    {
        return $this->userRepository->getById($userId);
    }
}