<?php
/**
 * Date: 28.12.17
 * Time: 18:03
 */

namespace App\Http\User\Services;

use App\Http\User\Repositories\UserRepository;
use Illuminate\Events\Dispatcher;

/**
 * Class DeleteUserService
 *
 * @package App\Http\User\Services
 * @author  Oleksii Griban
 */
class DeleteUserService
{
    /** @var Dispatcher  */
    private $dispatcher;

    /** @var UserRepository  */
    private $userRepository;

    /**
     * CreateUserService constructor.
     *
     * @param Dispatcher     $dispatcher
     * @param UserRepository $userRepository
     */
    public function __construct(
        Dispatcher $dispatcher,
        UserRepository $userRepository
    ) {
        $this->dispatcher = $dispatcher;
        $this->userRepository = $userRepository;
    }

    /**
     * @param $id
     * @return string
     */
    public function delete($id)
    {
        $this->userRepository->delete($id);
    }
}