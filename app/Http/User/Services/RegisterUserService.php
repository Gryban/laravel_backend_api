<?php
/**
 * Date: 28.12.17
 * Time: 18:03
 */

namespace App\Http\User\Services;

use App\Http\Auth\Proxy\LoginProxy;
use App\Http\User\Events\UserWasCreated;
use App\Http\User\Repositories\UserRepository;
use Illuminate\Events\Dispatcher;

/**
 * Class RegisterUserService
 *
 * @package App\Http\User\Services
 * @author  Oleksii Griban
 */
class RegisterUserService
{
    /** @var Dispatcher  */
    private $dispatcher;

    /** @var UserRepository  */
    private $userRepository;

    /** @var LoginProxy  */
    private $loginProxy;

    /**
     * CreateUserService constructor.
     *
     * @param Dispatcher     $dispatcher
     * @param UserRepository $userRepository
     * @param LoginProxy     $loginProxy
     */
    public function __construct(
        Dispatcher $dispatcher,
        UserRepository $userRepository,
        LoginProxy $loginProxy
    ) {
        $this->dispatcher = $dispatcher;
        $this->userRepository = $userRepository;
        $this->loginProxy = $loginProxy;
    }

    /**
     * @param $data
     * @return array
     */
    public function create($data)
    {
        $user = $this->userRepository->create($data);

        $this->dispatcher->fire(new UserWasCreated($user));

        return ['access_token' => $this->loginProxy->token($user)];
    }
}