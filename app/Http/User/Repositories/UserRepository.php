<?php
/**
 * Created by Skynix Team
 * Date: 28.12.17
 * Time: 18:55
 */

namespace App\Http\User\Repositories;

use App\Http\Repository;
use App\User;

/**
 * Class UserRepository
 *
 * @package App\Http\User\Repositories
 * @author  Oleksii Griban
 */
class UserRepository extends Repository
{

    public function getModel()
    {
        return new User();
    }

    /**
     * @param array $data
     * @return User
     * @throws \Exception
     */
    public function create(array $data) : User
    {
        $user = $this->getModel();
        $data['password'] = password_hash($data['password'], PASSWORD_BCRYPT);
        $user->fill($data);
        $user->save();
        return $user;
    }

    /**
     * @param int $id
     * @param $request
     * @return mixed
     */
    public function update(int $id, $request)
    {
        return User::where('id', $id)->update($request);
    }

}