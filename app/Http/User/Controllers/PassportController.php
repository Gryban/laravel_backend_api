<?php
/**
 * Created by Skynix Team
 * Date: 28.12.17
 * Time: 17:52
 */

namespace App\Http\User\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Response;
use App\Http\User\Requests\LoginUserRequest;
use App\Http\User\Services\RegisterUserService;
use App\Http\User\Requests\CreateUserRequest;
use App\Http\User\Services\LoginUserService;

/**
 * Class PassportController
 *
 * @package App\Http\User\Controllers
 * @author  Oleksii Griban
 */
class PassportController extends Controller
{
    /**
     * @param CreateUserRequest   $request
     * @param RegisterUserService $userService
     * @return Response
     */
    public function register(CreateUserRequest $request, RegisterUserService $userService)
    {
        return new Response($userService->create($request->all()), 201);
    }

    /**
     * @param LoginUserRequest $request
     * @param LoginUserService $userService
     * @return Response
     */
    public function login(LoginUserRequest $request, LoginUserService $userService)
    {
        return new Response($userService->login($request->all()), 200);
    }
}