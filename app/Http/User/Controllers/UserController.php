<?php
/**
 * Created by Skynix Team
 * Date: 28.12.17
 * Time: 17:52
 */

namespace App\Http\User\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Response;
use App\Http\User\Requests\UpdateUserRequest;
use App\Http\User\Services\DeleteUserService;
use App\Http\User\Services\FetchUserService;
use App\Http\User\Services\UpdateUserService;
use App\Http\User\Services\ViewUserService;

/**
 * Class UserController
 *
 * @package App\Http\User\Controllers
 * @author  Oleksii Griban
 */
class UserController extends Controller
{
    /**
     * @param $id
     * @param ViewUserService $userService
     * @return Response
     */
    public function view($id, ViewUserService $userService)
    {
        $user = $userService->view($id);

        return new Response($user, 200);
    }

    /**
     * @param int $id
     * @param UpdateUserService $userService
     * @param UpdateUserRequest $request
     * @return Response
     */
    public function update(int $id, UpdateUserService $userService, UpdateUserRequest $request)
    {
        return new Response($userService->update($id, $request->all()), 200);
    }

    /**
     * @param $id
     * @param DeleteUserService $userService
     * @return Response
     */
    public function delete($id, DeleteUserService $userService)
    {
        return new Response($userService->delete($id), 200);
    }

    /**
     * @param FetchUserService $userService
     * @return Response
     */
    public function fetch(FetchUserService $userService)
    {
        return new Response($userService->fetch(), 200);
    }
}