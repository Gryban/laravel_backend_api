<?php
/**
 * Created by Skynix Team
 * Date: 30.03.18
 * Time: 16:32
 */

namespace App\Http\User\Events;

use App\User;

/**
 * Class UserWasCreated
 *
 * @package App\Http\User\Events
 * @author  Oleksii Griban
 */
class UserWasCreated
{
    public function __construct(User $user)
    {

    }



}