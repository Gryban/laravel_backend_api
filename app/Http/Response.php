<?php
/**
 * Created by PhpStorm.
 * User: z29105
 * Date: 22.05.2018
 * Time: 23:38
 */

namespace App\Http;

use Illuminate\Contracts\Support\Responsable;

/**
 * Class Response
 * @package App\Http
 */
class Response implements Responsable
{
    /** @var array  */
    public $response;

    /** @var int  */
    public $status;

    /**
     * Response constructor.
     * @param array $response
     * @param int $status
     */
    public function __construct($response = array(), $status = 200)
    {
        $this->response = $response;
        $this->status = $status;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function toResponse($request)
    {
        $response['success'] = true;
        $response['data'] = $this->response ?: array();

        return response()->json($response, $this->status);
    }
}