<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

/**
 * Class UserDeleteTest
 *
 * @package Tests\Feature
 * @author  Oleksii Griban
 */
class UserDeleteTest extends TestCase
{

    use RefreshDatabase, DatabaseMigrations, Auth;

    function setUp()
    {
        parent::setUp(); // TODO: Change the autogenerated stub

        $this->setClient();
    }

    /**
     * @return void
     */
    function testUserDelete()
    {
        $this->user = factory(User::class)->create(([
            'name'  => 'Alex',
            'email' => 'alex@test.co'
        ]));

        $this->login();

        $response = $this->json('DELETE', 'api/v1/user/' . $this->user->id, [], $this->headers);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'success',
                'data'
            ]);
    }
}