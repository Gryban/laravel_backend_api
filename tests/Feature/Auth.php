<?php
/**
 * Created by PhpStorm.
 * User: z29105
 * Date: 23.05.2018
 * Time: 23:49
 */

namespace Tests\Feature;

use Faker\Provider\cs_CZ\DateTime;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\ClientRepository;

trait Auth
{
    /** @var   */
    public $token;

    /** @var   */
    public $headers;

    /** @var   */
    public $user;

    public function setClient()
    {
        $clientRepository = new ClientRepository();
        $client = $clientRepository->createPersonalAccessClient(
            null, 'Test Personal Access Client', url('/')
        );
        DB::table('oauth_personal_access_clients')->insert([
            'client_id' => $client->id,
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime(),
        ]);
    }

    public function login()
    {
        $token = $this->user->createToken('Laravel')->accessToken;
        $this->headers['Authorization'] = 'Bearer ' . $token;
    }

}