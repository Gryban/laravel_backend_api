<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

/**
 * Class UserFetchTest
 *
 * @package Tests\Feature
 * @author  Oleksii Griban
 */
class UserFetchTest extends TestCase
{

    use RefreshDatabase, DatabaseMigrations;

    /**
     * @return void
     */
    function testGetUserFetch()
    {
        factory(User::class)->create((
            [
                'name'  => 'Alex',
                'email' => 'alex@test.co'
            ]
        ));

        factory(User::class)->create((
        [
            'name'  => 'Alex',
            'email' => 'alex-test@test.co'
        ]
        ));

        $response = $this->json('GET', 'api/v1/users/');

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'success',
                'data' => [
                    [
                        'id',
                        'email',
                        'name',
                        'created_at',
                        'updated_at'
                    ]
                ]
            ]);
    }
}