<?php

use \Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// without authentication
Route::group(['namespace' => 'User\Controllers', 'prefix' => '/v1', 'middleware' => ['api']], function() {

    Route::post('/register', ['as' => 'passport.register', 'uses' => 'PassportController@register']);
    Route::post('/login', ['as' => 'user.fetch', 'uses' => 'PassportController@login']);

    Route::get('/users', ['as' => 'user.fetch', 'uses' => 'UserController@fetch']);
});

// with authentication
Route::group(['namespace' => 'User\Controllers', 'prefix' => '/v1', 'middleware' => ['auth:api']], function() {
    Route::get('/user/{id}', ['as' => 'user.view', 'uses' => 'UserController@view']);
    Route::put('/user/{id}', ['as' => 'user.update', 'uses' => 'UserController@update']);
    Route::delete('/user/{id}', ['as' => 'user.delete', 'uses' => 'UserController@delete']);

});