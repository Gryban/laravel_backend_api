LARAVEL 5.6.4 - last version

1. Роути
    - GET    /api/v1/users     - fetch users
    - GET    /api/v1/user/1    - view user by id
    - PUT    /api/v1/user/1    - update user by id
    - DELETE /api/v1/users     - delete user by id
    - POST   /api/v1/register  - register new user
    - POST   /api/v1/login     - login by email, password

2. Авторизація
    - Регістрація через модуль - laravel passport
    - Логін - передаємо в хедері  "Authorization : Bearer <token>"

3. Структура папок проекту
    - app
        - Http
            - User
                - Controllers
                - Events
                - Exception
                - Repositories
                - Requests
                - Services
                
4. Організація проекту (REST APi)
    - проект росподіляється по основним бізнесс-моделям, наприклад папка User і в ній вся логіка, що пов'язанна з юзерами
    - авторизація для запитів встановлюється в роутах, відбувається через middleware модулю laravel passport
    - в контролеррах ніякої логіки тільки визови реквестів і сервісів
    - валідацію робити тільки в реквестах а також отримання данних з запиту, наприклад CreateUserRequest 
    - бізнес-логіку розташовувати у сервісах, наприклад CreateUserService
    - розподл по ролям та визначення прав доступу виконується в класі Policy
    - також використовувати для відповідних цілей app/Http/User/Events, app/Http/User/Exceptions, app/Http/User/Jobs та інші
    - всі запити до бд робити тільки з репозиторія, наприклад UserRepository
    - вивід успіху описується в классі app/Http/Response
    - вивід помилок у классі app/Exceptions/Handler
    - тести в tests/Feature, беруться тестові данні з .env.testing(тестова бд та інше), схема робиться основними міграціями, після кожного тесту все затирається
            

